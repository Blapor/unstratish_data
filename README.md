# unstratish_data

The training and test data for [unstratish](https://gitlab.com/Blacksilver/Unstratish)

Train.csv should be in alphabetical order to avoid merge conflicts.
